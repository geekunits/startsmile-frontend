$(function() {

	//
	// selects 
	//

	$('.p-newsearch__select').each(function(){
		var $this = $(this);

		$this.select2({
			dropdownCssClass: $this.data('dropdownclass'),
			placeholder: $(this).data('placeholder'),
			allowClear: true,
			dropdownAutoWidth : true,
			templateResult: formatCustomState,
			dropdownPosition: 'below'
		}).on("select2:unselecting", function (e) {
			$(this).data('unselecting', true);
		}).on('select2:open', function(e){
			// $('.select2-results__options').mCustomScrollbar('destroy').mCustomScrollbar('update');

			// setTimeout(function () {
			// 	$('.select2-results__options').mCustomScrollbar({
			// 		alwaysShowScrollbar: 1
			// 	});
			// }, 0);

			$('.select2-results__options').niceScroll({
				cursorcolor: "#FF9F00",
				cursorborder: "1px solid #FF9F00",
				background: "#f5f5f5",
				cursorwidth: "2px",
				cursorborderradius: "5px",
				autohidemode: false
			});

			if ($(this).data('unselecting')) {
				$(this).select2('close').removeData('unselecting');
			}
		});
	});

	function formatCustomState (state) {
		if (!state.id) {
			return state.text;
		}

		var el = state.element;
		var $state = ( $(el).data('color') ) ? $('<div class="select-item-color select-item-color_' + $(el).data('color')  + '">' + state.text + '</div>') : state.text;

		return $state;
	};

	/* selects ... end; */

	/*
	*	search-block more btn
	*/

	$(".search-block__bot-more-btn").on('click', function(e){
		e.preventDefault();
		var $this = $(this);

		$this.toggleClass('open');
		$('.search-block__filters-wrap').toggleClass('open');
	});

	// filters xs btn
	$(".search-block__mid-filters-btn").on('click', function(e){
		e.preventDefault();
		var $this = $(this);

		$this.toggleClass('open');
		$('.search-block__bot').toggleClass('open');
	});

	/*
	* search-block owl items
	*/

	$('.owl-search').owlCarousel({
		nav:true,
		items: 1,
		margin: 0,
		singleItem: true,
		dots:false,
		loop: true
	});

	/*
	* sort xs btn
	*/ 

	$(".search-block__sort-title-xs").on("click", function(e){
		e.preventDefault();
		var $this = $(this);

		$this.toggleClass('open');
		$this.siblings('.search-block__sort-list-xs').fadeToggle(300);
	});

	$(".search-block__sort-list-xs .search-block__sort-link").on("click", function(e){
		var $this = $(this);
		var text = $this.html();
		$(".search-block__sort-list-xs .search-block__sort-item").removeClass("active");

		$this.parent().addClass('active');
		$this.parents('.search-block__sort').find(".search-block__sort-title-xs").toggleClass('open').html(text);
		$this.parents('.search-block__sort-list-xs').fadeToggle(300);
	});

	/*
	* yandex map
	*/ 

	if ( $('#maplg').length || $('#mapsm').length ) {
		ymaps.ready(initMaps);

		var mapOptions = {
			center: [55.752220, 37.615560],
			zoom: 15,
			controls: []
		};

		function initMapById(id){
			var myMap = new ymaps.Map(id, mapOptions, {
				searchControlProvider: 'yandex#search'
			});

			//Marks
			// var orangeMark = new ymaps.Placemark([55.755358, 37.643077], {
			// 	hintContent: 'Клиника'
			// }, {
			// 	iconLayout: 'default#image',
			// 	iconImageHref: '../images/mark.png',
			// 	iconImageSize: [23, 23],
			// 	iconImageOffset: [0, 0]
			// });

			// var greenMark = new ymaps.Placemark([55.755358, 37.643077], {
			// 	hintContent: 'Клиника'
			// }, {
			// 	iconLayout: 'default#image',
			// 	iconImageHref: '../images/mark.png',
			// 	iconImageSize: [23, 23],
			// 	iconImageOffset: [0, 0]
			// });

			// var orangeMarkCount = new ymaps.Placemark([55.755358, 37.643077], {
			// 	hintContent: 'Клиника',
			// 	iconContent: '5'
			// }, {
			// 	iconLayout: 'default#image',
			// 	iconImageHref: '../images/mark.png',
			// 	iconImageSize: [23, 23],
			// 	iconImageOffset: [0, 0]
			// });

			// myMap.geoObjects.add(orangeMark).add(greenMark).add(orangeMarkCount);
			//Marks ..end;

			myMap.behaviors.disable('scrollZoom');
		};

		function initMaps () {
			initMapById('maplg');
			initMapById('mapsm');
		};
	};

	/* 
	* sticky map
	*/

	var $maplg = $('#maplg');
	var $searchBlock = $('#search-block');

	if ( $maplg.length && $searchBlock.length ) {

		$(window).scroll(ascroll);

		function ascroll() {
			var maplgCoord = $searchBlock[0].getBoundingClientRect();
			var stopvalue = Math.round(maplgCoord.top + $maplg[0].getBoundingClientRect().height - $searchBlock[0].getBoundingClientRect().bottom);

			if ( maplgCoord.top <= 0) {
				if ( maplgCoord.top <= stopvalue ) {
					$maplg.removeClass('map-sticky').addClass('map-stop');
					$maplg[0].style.top = - stopvalue + 'px';
				} else {
					$maplg.removeClass('map-stop').addClass('map-sticky');
					$maplg[0].style.top = 0;
				}
			} else {
				$maplg.removeClass('map-stop, map-sticky');
			}
		};
	};

	/*
	* search mobile controls
	*/

	$('.search-block__m-c-filters').on('click', function(e){
		e.preventDefault();

		$(this).toggleClass('active');
		$('.search-block__form').stop(true,true).animate({
			height: "toggle",
			opacity: "toggle"
		});
	});

	$('.search-block__m-c-map').on('click', function(e){
		e.preventDefault();

		$(this).addClass('active');
		$('.search-block__m-c-list').removeClass('active');
		$('.search-block__title').hide();
		$('.search-block__top').hide();
		$('.search-block__controls').addClass('map-active');
		$('.main>section:not(.search-block)').hide();
		$('body>footer').hide();
		$('.search-block__sort').hide();
		$('.search-block__list').hide();
		$('.search-block__more-item-btn').hide();

		$('body, .main, .search-block__container-wrap, .search-block, .search-block__container, .search-block__form, html').addClass('map-active');

		$('.search-block__map-container').show();
		$('.search-block__m-c-gps').show();
	});

	$('.search-block__m-c-list').on('click', function(e){
		e.preventDefault();

		$(this).addClass('active');
		$('.search-block__m-c-map').removeClass('active');

		$('.search-block__title').show();
		$('.search-block__top').show();
		$('.search-block__controls').removeClass('map-active');

		$('.main>section:not(.search-block)').show();
		$('body>footer').show();
		$('.search-block__sort').show();
		$('.search-block__list').show();
		$('.search-block__more-item-btn').show();
		$('body, .main, .search-block__container-wrap, .search-block, .search-block__container, .search-block__form, html').removeClass('map-active');

		$('.search-block__map-container').hide();
		$('.search-block__m-c-gps').hide();
	});

	/*
	* map modals
	*/

	$('.owl-map-modal-solo').owlCarousel({
		nav:true,
		dots:false,
		margin:0,
		items:1,
		loop:true
	});

	$('.owl-map-modal-xs').owlCarousel({
		nav:false,
		dots:true,
		margin:10,
		items:1,
		loop:true
	});

	if ( $(".map-modal-list__bot-wrap").length ) {
		$(".map-modal-list__bot-wrap").mCustomScrollbar();
	}

	//
	// heart btn
	//

	var $heartFavorBtn = $('.search-block__item-left-heart');
	$heartFavorBtn.on('click', function(e){ e.preventDefault(); });
	$heartFavorBtn.on('click', addToFavorite);

	function addToFavorite(){
		var $this = $(this);

		$this.toggleClass('active show-heart-text').off('click', addToFavorite);
		setTimeout(function(){
			$this.removeClass('show-heart-text').on('click', addToFavorite);
		}, 1500);
	};

	/* heart btn ...end; */

	//
	// search items shadow
	//

	var $adressInItems = $('.search-block__item-adress');

	$adressInItems.on('scroll', function(){
		var $this = $(this);
		var scrollLeft = $this.scrollLeft();
		var w = $this.width();
		var sw = this.scrollWidth;

		if ( sw > w ) {
			$this.parent().addClass('add-right-shadow');

			if ( scrollLeft == sw - w ) {
				$this.parent().removeClass('add-right-shadow');
			}
		};
	}).scroll();

	var $itemTitle = $('.search-block__item-title');

	$itemTitle.each(function() {
		var $this = $(this);
		var h = $this.height();
		var $parent = $this.closest('.search-block__item-title-c');
		var parH = $parent.height();

		if ( h > parH ) {
			$parent.addClass('add-right-shadow');
		} else {
			$parent.removeClass('add-right-shadow');
		}
	});

	/* search items shadow ... end; */

	/*
	* search new page
	*/

	$('.p-newsearch__btn-drop-toggle').on('click', function(e){
		e.preventDefault();
		var $this = $(this);
		var $siblingDrop = ( $this.data('target') ) ? $($this.data('target')) : $this.siblings('.p-newsearch__inputs-drop');
		var $inputsDrop = $('.p-newsearch__inputs-drop').not($siblingDrop);
		var $toggleBtns = $('.p-newsearch__btn-drop-toggle');

		$inputsDrop.removeClass('active').hide(0);
		$toggleBtns.not($this).removeClass('active');
		$this.toggleClass('active');
		$siblingDrop.stop().toggle(0).toggleClass('active');
	});

	$(document).on('click', function(e){
		var $inputsDrop = $('.p-newsearch__inputs-drop');
		var $toggleBtn = $('.p-newsearch__btn-drop-toggle');

		if (!$inputsDrop.is(e.target) && $inputsDrop.has(e.target).length === 0 && !$toggleBtn.is(e.target)) { 
			$inputsDrop.removeClass('active').hide(0);
			$toggleBtn.removeClass('active');
		} 
	});

	var $syncInputsBtns = $('[data-sync-input="true"]');

	$syncInputsBtns.each(function() {
		var $this = $(this);
		var target = $this.data('target');
		var $inputs = ( target ) ? $(target).find('input') : $this.siblings('.p-newsearch__inputs-drop').find('input');
		var placeholder = $this.data('placeholder');
		
		$inputs.on('change', function(e){
			var arr = [];
			var str = '';

			$inputs.each(function(i, el){
				if (el.checked) arr.push( $(el).siblings('span').text() );
			});

			str = arr.join(', ');

			( str ) ? $this.addClass('has_value').html(str) : $this.removeClass('has_value').html(placeholder);
		});
	});

	var $clearInputsBtn = $('.p-newsearch__input-clear-btn');

	$clearInputsBtn.on('click', function(e){
		e.preventDefault();
		e.stopPropagation();

		var $this = $(this);
		var $toggleBtns = $('.p-newsearch__btn-drop-toggle');
		var $inputBtn = $this.siblings('.p-newsearch__btn-drop-toggle');
		var target = $inputBtn.data('target');
		var $inputs = ( target ) ? $(target).find('input') : $inputBtn.siblings('.p-newsearch__inputs-drop').find('input');
		var placeholder = $inputBtn.data('placeholder');
		var $inputsDrop = $('.p-newsearch__inputs-drop');

		var isRadio = $inputs.attr('type') == 'radio';
		
		( isRadio ) ? $inputs[0].click() : $inputs.prop('checked', false).trigger('change');

		$inputsDrop.removeClass('active').hide(0);
		$toggleBtns.removeClass('active');
	});

	var $specInputs = $('[data-spec-id]');
	var specDefVal = $('[data-spec-id]:checked');
	var $uslugiCont = $('#p-newsearch__btn-uslug-c');
	var $usligiTabs = $('.p-newsearch__uslug-tab');
	var $usligiXsTabs = $('.p-newsearch__select-xs-group');

	$specInputs.on('change', function(e){
		var $this = $(this);
		var target = $this.data('spec-id');

		( target == "spec-0" ) ? $uslugiCont.addClass('p-newsearch__input-c_disabled') : $uslugiCont.removeClass('p-newsearch__input-c_disabled');
		$usligiTabs.removeClass('active').find('input').prop('checked', false).trigger('change');
		$('[data-uslug-id=' + target + ']').addClass('active');
	});

	$specInputs.on('click', function(e){
		if ( $(this)[0] == specDefVal[0] ) {
			$specInputs[0].click();
			specDefVal = $specInputs[0];
		} else {
			specDefVal = $(this);
		};
	});

	var $xsSpecInputs = $('.p-newsearch__inputs-wrap-xs [data-spec-id]');

	$xsSpecInputs.parent().on('change', function(e){
		var $this = $(this);
		var $option = $this.find('option:selected');
		var target = $option.data('spec-id');
		var $uslugiInputsC = $('.p-newsearch__select-xs-c_usluga');

		$uslugiInputsC.show();

		$usligiXsTabs.removeClass('active');

		$('[data-uslug-id=' + target + ']').addClass('active');
	});

	if ( $('.p-newsearch').length ){
		$('.modal').addClass('color_orange_v2');
	};

	var $xsSrchSelects = $('.p-newsearch__select-xs');
	var $xsSrchClearBtn = $('.p-newsearch__select-xs-clear-btn');

	$xsSrchSelects.on('change', function(e){
		var $this        = $(this);
		var index        = $this[0].selectedIndex;
		var selOptions   = $this[0].selectedOptions;
		var selOptLength = selOptions.length;
		var selected     = selOptions[0].innerHTML;
		var placeholder  = $this.data('placeholder');
		var label        = $this.siblings('.p-newsearch__select-xs-multiple-label');

		( index != -1 ) ? $this.addClass('active') : $this.removeClass('active');

		if ( selOptLength == 1 ) {
			label.text(selected);
		} else {
			label.text(placeholder + " (" + selOptLength + ")");
		};
	});

	$xsSrchClearBtn.on('click', function(e){
		e.preventDefault();

		var $this = $(this);
		var $select = $this.siblings('select');
		var placeholder = $select.data('placeholder');
		var label = $select.siblings('.p-newsearch__select-xs-multiple-label');

		$select[0].selectedIndex = -1;
		$select.removeClass('active');
		label.text(placeholder);

		if ( $this.is('.p-newsearch__select-xs-clear-btn_spec') ) {
			$('.p-newsearch__select-xs-c_usluga').hide();
		};
	});

	// new controls

	$('.p-newsearch__bot-c-map').on('click', function(e){
		e.preventDefault();

		$(this).addClass('active');
		$('.p-newsearch__bot-c-list').removeClass('active');
		$('.p-newsearch-top__title').hide();
		$('.p-newsearch-top__desc').hide();
		$('.main .p-newsearch>section:not(.search-block, .p-newsearch-top)').hide();
		$('body>footer').hide();
		$('.search-block__sort').hide();
		$('.search-block__list').hide();
		$('.search-block__more-item-btn').hide();
		$('.search-block__map-container').show();
		$('.p-newsearch__bot-c-gps').show();

		$('body, .main, .search-block__container-wrap, .search-block, .search-block__container, .search-block__form, html, .p-newsearch').addClass('map-active');
	});

	$('.p-newsearch__bot-c-list').on('click', function(e){
		e.preventDefault();

		$(this).addClass('active');
		$('.p-newsearch__bot-c-map').removeClass('active');

		$('.p-newsearch-top__title').show();
		$('.p-newsearch-top__desc').show();
		$('.main .p-newsearch>section:not(.search-block, .p-newsearch-top)').show();
		$('body>footer').show();
		$('.search-block__sort').show();
		$('.search-block__list').show();
		$('.search-block__more-item-btn').show();
		$('body, .main, .search-block__container-wrap, .search-block, .search-block__container, .search-block__form, html, .p-newsearch').removeClass('map-active');

		$('.search-block__map-container').hide();
		$('.p-newsearch__bot-c-gps').hide();
	});

	
});