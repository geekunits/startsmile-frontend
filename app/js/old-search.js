$(function() {

	//
	// selects 
	//

	$('.search-block__bot-select.town').select2({
		dropdownCssClass: "search-block-dropdown-town"
	});

	$('.search-block__bot-select.metro').select2({
		dropdownCssClass: "search-block-dropdown-metro"
	});

	$('.search-block__bot-select.okrug').select2({
		dropdownCssClass: "search-block-dropdown-okrug"
	});

	$('.search-block__bot-select.destrict').select2({
		dropdownCssClass: "search-block-dropdown-destrict"
	});

	/* selects ... end; */

	/*
	*	search-block more btn
	*/

	$(".search-block__bot-more-btn").on('click', function(e){
		e.preventDefault();
		var $this = $(this);

		$this.toggleClass('open');
		$('.search-block__filters-wrap').toggleClass('open');
	});

	// filters xs btn
	$(".search-block__mid-filters-btn").on('click', function(e){
		e.preventDefault();
		var $this = $(this);

		$this.toggleClass('open');
		$('.search-block__bot').toggleClass('open');
	});

	/*
	* search-block owl items
	*/

	$('.owl-search').owlCarousel({
		nav:true,
		items: 1,
		margin: 0,
		singleItem: true,
		dots:false,
		loop: true
	});

	/*
	* sort xs btn
	*/ 

	$(".search-block__sort-title-xs").on("click", function(e){
		e.preventDefault();
		var $this = $(this);

		$this.toggleClass('open');
		$this.siblings('.search-block__sort-list-xs').fadeToggle(300);
	});

	$(".search-block__sort-list-xs .search-block__sort-link").on("click", function(e){
		var $this = $(this);
		var text = $this.html();
		$(".search-block__sort-list-xs .search-block__sort-item").removeClass("active");

		$this.parent().addClass('active');
		$this.parents('.search-block__sort').find(".search-block__sort-title-xs").toggleClass('open').html(text);
		$this.parents('.search-block__sort-list-xs').fadeToggle(300);
	});

	/*
	* yandex map
	*/ 

	if ( $('#maplg').length || $('#mapsm').length ) {
		ymaps.ready(initMaps);

		var mapOptions = {
			center: [55.752220, 37.615560],
			zoom: 15,
			controls: []
		};

		function initMapById(id){
			var myMap = new ymaps.Map(id, mapOptions, {
				searchControlProvider: 'yandex#search'
			});

			//Marks
			// var orangeMark = new ymaps.Placemark([55.755358, 37.643077], {
			// 	hintContent: 'Клиника'
			// }, {
			// 	iconLayout: 'default#image',
			// 	iconImageHref: '../images/mark.png',
			// 	iconImageSize: [23, 23],
			// 	iconImageOffset: [0, 0]
			// });

			// var greenMark = new ymaps.Placemark([55.755358, 37.643077], {
			// 	hintContent: 'Клиника'
			// }, {
			// 	iconLayout: 'default#image',
			// 	iconImageHref: '../images/mark.png',
			// 	iconImageSize: [23, 23],
			// 	iconImageOffset: [0, 0]
			// });

			// var orangeMarkCount = new ymaps.Placemark([55.755358, 37.643077], {
			// 	hintContent: 'Клиника',
			// 	iconContent: '5'
			// }, {
			// 	iconLayout: 'default#image',
			// 	iconImageHref: '../images/mark.png',
			// 	iconImageSize: [23, 23],
			// 	iconImageOffset: [0, 0]
			// });

			// myMap.geoObjects.add(orangeMark).add(greenMark).add(orangeMarkCount);
			//Marks ..end;

			myMap.behaviors.disable('scrollZoom');
		};

		function initMaps () {
			initMapById('maplg');
			initMapById('mapsm');
		};
	};

	/* 
	* sticky map
	*/

	var $maplg = $('#maplg');
	var $searchBlock = $('#search-block');

	if ( $maplg.length && $searchBlock.length ) {

		$(window).scroll(ascroll);

		function ascroll() {
			var maplgCoord = $searchBlock[0].getBoundingClientRect();
			var stopvalue = Math.round(maplgCoord.top + $maplg[0].getBoundingClientRect().height - $searchBlock[0].getBoundingClientRect().bottom);

			if ( maplgCoord.top <= 0) {
				if ( maplgCoord.top <= stopvalue ) {
					$maplg.removeClass('map-sticky').addClass('map-stop');
					$maplg[0].style.top = - stopvalue + 'px';
				} else {
					$maplg.removeClass('map-stop').addClass('map-sticky');
					$maplg[0].style.top = 0;
				}
			} else {
				$maplg.removeClass('map-stop, map-sticky');
			}
		};
	};

	/*
	* search mobile controls
	*/

	$('.search-block__m-c-filters').on('click', function(e){
		e.preventDefault();

		$(this).toggleClass('active');
		$('.search-block__form').stop(true,true).animate({
			height: "toggle",
			opacity: "toggle"
		});
	});

	$('.search-block__m-c-map').on('click', function(e){
		e.preventDefault();

		$(this).addClass('active');
		$('.search-block__m-c-list').removeClass('active');
		$('.search-block__title').hide();
		$('.search-block__top').hide();
		$('.search-block__controls').addClass('map-active');
		$('.main>section:not(.search-block)').hide();
		$('body>footer').hide();
		$('.search-block__sort').hide();
		$('.search-block__list').hide();
		$('.search-block__more-item-btn').hide();

		$('body, .main, .search-block__container-wrap, .search-block, .search-block__container, .search-block__form, html').addClass('map-active');

		$('.search-block__map-container').show();
		$('.search-block__m-c-gps').show();
	});

	$('.search-block__m-c-list').on('click', function(e){
		e.preventDefault();

		$(this).addClass('active');
		$('.search-block__m-c-map').removeClass('active');

		$('.search-block__title').show();
		$('.search-block__top').show();
		$('.search-block__controls').removeClass('map-active');

		$('.main>section:not(.search-block)').show();
		$('body>footer').show();
		$('.search-block__sort').show();
		$('.search-block__list').show();
		$('.search-block__more-item-btn').show();
		$('body, .main, .search-block__container-wrap, .search-block, .search-block__container, .search-block__form, html').removeClass('map-active');

		$('.search-block__map-container').hide();
		$('.search-block__m-c-gps').hide();
	});

	/*
	* map modals
	*/

	$('.owl-map-modal-solo').owlCarousel({
		nav:true,
		dots:false,
		margin:0,
		items:1,
		loop:true
	});

	$('.owl-map-modal-xs').owlCarousel({
		nav:false,
		dots:true,
		margin:10,
		items:1,
		loop:true
	});

	if ( $(".map-modal-list__bot-wrap").length ) {
		$(".map-modal-list__bot-wrap").mCustomScrollbar();
	}



	//
	// heart btn
	//

	var $heartFavorBtn = $('.search-block__item-left-heart');
	$heartFavorBtn.on('click', function(e){ e.preventDefault(); });
	$heartFavorBtn.on('click', addToFavorite);

	function addToFavorite(){
		var $this = $(this);

		$this.toggleClass('active show-heart-text').off('click', addToFavorite);
		setTimeout(function(){
			$this.removeClass('show-heart-text').on('click', addToFavorite);
		}, 1500);
	};

	/* heart btn ...end; */



	//
	// search items shadow
	//

	var $adressInItems = $('.search-block__item-adress');

	$adressInItems.on('scroll', function(){
		var $this = $(this);
		var scrollLeft = $this.scrollLeft();
		var w = $this.width();
		var sw = this.scrollWidth;

		if ( sw > w ) {
			$this.parent().addClass('add-right-shadow');

			if ( scrollLeft == sw - w ) {
				$this.parent().removeClass('add-right-shadow');
			}
		};
	}).scroll();

	var $itemTitle = $('.search-block__item-title');

	$itemTitle.each(function() {
		var $this = $(this);
		var h = $this.height();
		var $parent = $this.closest('.search-block__item-title-c');
		var parH = $parent.height();

		if ( h > parH ) {
			$parent.addClass('add-right-shadow');
		} else {
			$parent.removeClass('add-right-shadow');
		}
	});

	/* search items shadow ... end; */

});