$(function() {

	var screenIs = {
		xs : window.innerWidth < 767
	};

	/*
	* dropdown easy in out 
	*/

	$('.header-mid__item .dropdown, .header-nav__item .dropdown').hover(function() {
		var $this = $(this);

		setTimeout(function(){
			$this.addClass('open');
		},100);
		$this.find('.dropdown-content').stop(true, true).delay(100).fadeIn(300, 'easeInOutCubic');
	}, function() {
		var $this = $(this);

		setTimeout(function(){
			$this.removeClass('open');
		},100);
		$this.find('.dropdown-content').stop(true, true).delay(100).fadeOut(300, 'easeInOutCubic');
	});

	/*
	* owl main
	*/

	$(".owl-blmain").owlCarousel({
		nav:false,
		dots:true,
		margin:0,
		items: 1,
		loop: true,
		autoplay:true,
		autoplayHoverPause:true
	});

	/*
	* stars rating
	*/

	$('.best__card-bot-rating').each(function() {
		var $this = $(this);
		var val = parseFloat( $this.find('.best__c-b-r-count').html() );
		var starSize = $this.find('li').outerWidth(true);
		var size = Math.max(0, (Math.min(5, val))) * starSize;
		
		$this.find('.best__c-b-r-stars.best__c-b-r-stars__orange').width(size);
	});

	if (window.matchMedia("(min-width: 768px)").matches) {
		$('.doc-main__feedbacks-choose-item .best__c-b-r-star').hover(function(e){
			var $this = $(this);
			var val = parseFloat( $this.index()+1 );
			var starSize = $this.outerWidth(true);
			var size = Math.max(0, (Math.min(5, val))) * starSize +"px";
			var $hoverStars = $this.parent().parent().find('.best__c-b-r-stars.best__c-b-r-stars__orange');

			$hoverStars.css({"width" : size, "opacity" : 0.3}); 
		},
		function(){
			var $this = $(this);
			var $hoverStars = $this.parent().parent().find('.best__c-b-r-stars.best__c-b-r-stars__orange');
			var oldSize = $this.parent().parent().find('.best__c-b-r-stars').attr("data-width") || 0;

			$hoverStars.css({"width" : oldSize, "opacity" : 1});
		});
	};

	$('.doc-main__feedbacks-choose-item .best__c-b-r-star').on("click", function(){
		var $this = $(this);
		var val = parseFloat( $this.index()+1 );
		var starSize = $this.outerWidth(true);
		var size = Math.max(0, (Math.min(5, val))) * starSize +"px";
		var index = $this.index();
		var $hoverStars = $this.parent().parent().find('.best__c-b-r-stars.best__c-b-r-stars__orange');
		var $input = $this.parents('.doc-main__feedbacks-choose-item').find('.doc-main__star-input');
		var $list = $this.parents('.doc-main__feedbacks-choose-item').find('.doc-main__feedbacks-choose-desc');
		
		$input.val(val);
		$list.find(".doc-main__feedbacks-choose-desc-item").hide().eq(index).fadeIn(300, 'easeInOutCubic');
		$this.parent().parent().find('.best__c-b-r-stars').attr("data-width", size);
		$hoverStars.css({"width" : size, "opacity" : 1});
	});
	
	/*
	* select2
	*/

	$('.main-cost__select').select2();

	/*
	* main-video
	*/

	$('.owl-video').owlCarousel({
		mouseDrag:false,
		nav:false,
		items: 1,
		margin: 5,
		singleItem: true,
		dots:false,
		thumbs: true,
		thumbsPrerendered: true
	});

	$('.article-p__owl-video').owlCarousel({
		nav:false,
		dots:false,
		items: 1,
		margin: 5,
		singleItem: true
	});

	$('.owl-video__cover').on("click", function(e){
		e.preventDefault();
		var $this = $(this);

		$this.hide().siblings('.owl-video__video').fadeIn(300, 'easeInOutCubic');
	});

	$('.owl-thumbs-video__img-container, .slick-thumbs__img-container').on('click', function(e){
		$(".owl-video__video").each(function(index, el) {
			el.pause();
		});
		$('.owl-item, .slick-video__item').find(".owl-video__cover").show().siblings('.owl-video__video').hide();
	});

	/*
	*	show all btn main bottom
	*/

	$('.allinfo__show-btn').on('click', function(e){
		e.preventDefault();
		var $this = $(this);

		$this.toggleClass('open').siblings('.allinfo__list').toggleClass('open');
	});

	/*
	*   inner modals
	*/ 

	var modals = (function(){

		var innerModal = $(".inner-modal");	
		var openButtons = $("[data-pop='true']"); 

		var init = function(){
			setUpListeners();
		};

		var setUpListeners = function(){
			innerModal.on("click", closePopup);
			openButtons.on("click", thatWillBeOpen);
		};

		function thatWillBeOpen(e){
			e.preventDefault();

			var $this = $(this);
			var href = $this.attr("href");

			openPopup(href);
		};

		function openPopup(target) { 
			$(target).fadeIn(200);
		};

		function closePopup(e) {
			var $target = $(e.target);

			if ($target.hasClass('inner-modal') || $target.attr('data-pop-close')) {
				innerModal.fadeOut(200);
			}
		};

		return {
			init:init
		}
	}());

	$(document).ready(function(){
		modals.init();
	});

	/*
	* auth buttons change tab in modals
	*/

	$('#header__auth-reg').on('click', function(e){
		$('#toggle-tab-sign').click();
	});

	$('#header__auth-sign').on('click', function(e){
		$('#toggle-tab-reg').click();
	});

	/*
	* dropdowns mobile menu
	*/ 

	//клик по бургеру
	$('.header-mid__menu-btn').on('click', function (e) {
		e.preventDefault();
		var $this = $(this);

		$this.parent().toggleClass('open');
		$('body').toggleClass('fixed');
		$this.siblings('.header-mid__menu-dropdown').first().stop(true, true).fadeToggle(300);
	});

	//клик по кнопке в меню
	$(".menu-dropdown__link[data-drop='true']").on("click", function(e){
		e.preventDefault();
		var $this = $(this);

		$this.addClass('open');
		$this.siblings('.menu-dropdown__item-drop').stop(true, true).fadeIn(300, 'easeInOutCubic');
	});

	//клик по кнопке в меню возвращающей на пред уровень
	$(".menu-dropdown__link.menu-dropdown__link_back").on("click", function(e){
		e.preventDefault();
		var $this = $(this);

		$this.parent().stop(true, true).fadeOut(300, 'easeInOutCubic');
		$this.parent().siblings(".menu-dropdown__link[data-drop='true']").removeClass('open');
	});

	/*
	* owl menu mobile
	*/

	$('.owl-menu').owlCarousel({
		nav:false,
		dots:false,
		margin:20,
		autoWidth:true
	});

	/*
	* doctor progress abilities
	*/

	$('.doc-main__feedbacks-abilities-progress-count').each(function() {
		var $this = $(this);
		var val = parseFloat( $this.html() );
		var max = 5;
		var size = val/max*100+"%";

		$this.siblings('.doc-main__feedbacks-abilities-progress').find('.doc-main__feedbacks-abilities-progress-bar').width(size);
	});

	/*
	* clinic material owl
	*/

	$('.owl-doc-mat').owlCarousel({
		nav:false,
		dots:true,
		margin:30,
		items: 1,
		responsive : {

			768 : {
				items: 1
			},

			992 : {
				items: 2
			},

			1200 : {
				items: 3
			}
		}
	});

	/*
	* scroll to..
	*/

	$('.doctor-p__nav-link').on("click", function(e){
		e.preventDefault();
		var $this = $(this);
		var $parentsLi = $('.doctor-p__nav-item');
		var href = $this.attr("href");
		var navHeight = 61;

		$("html, body").stop().animate({ scrollTop: $(href).offset().top - navHeight }, 1000);
	});

	/*
	* doctor owl navigat
	*/

	$('.owl-doctor-nav').owlCarousel({
		nav:false,
		dots:false,
		margin:30,
		autoWidth:true
	});

	/*
	* doctor owl near
	*/

	$('.doc-aside__docnear-owl').owlCarousel({
		nav:true,
		dots:true,
		margin:20,
		items:1,
		loop: true
	});

	/*
	* clinic owl near
	*/

	$('.doc-aside__clinnear-owl').owlCarousel({
		nav:true,
		dots:true,
		margin:20,
		items:1,
		loop: true
	});

	/*
	* clinic net owl
	*/

	$('.owl-doc-net').owlCarousel({
		nav:false,
		dots:true,
		margin:30,
		items: 1,
		loop:true,
		responsive : {
			768 : {
				items: 1
			},

			992 : {
				items: 2
			},

			1200 : {
				items: 3
			}
		}
	});
	
	/*
	* clinic photo
	*/

	$('.owl-main-photo').owlCarousel({
		nav:true,
		dots:true,
		margin:0,
		items: 1,
		loop:true
	});

	/*
	* feedback form
	*/

	$('.doc-main__feedbacks-attention').on('click', function(e){
		e.preventDefault();
		var $this = $(this);

		$this.toggleClass('open');
		$('.doc-main__feedbacks-attention-inner').toggleClass('open');
	});

	/*
	* owl-doc-doctors
	*/

	$('.owl-doc-doctors').owlCarousel({
		nav:false,
		dots:true,
		margin:30,
		items: 1,
		responsive : {
			768 : {
				items: 1
			},

			992 : {
				items: 2
			},

			1200 : {
				items: 3
			}
		}
	});

	/*
	* owl doc video 
	*/ 

	$('.owl-doc-video').owlCarousel({
		mouseDrag:false,
		nav:false,
		items: 1,
		margin: 5,
		singleItem: true,
		dots:false
	});

	$('.owl-doc-video-thumbs').owlCarousel({
		mouseDrag:false,
		nav:true,
		items: 2,
		margin: 20,
		dots:false,
		responsive : {
			768 : {
				items: 2
			},

			992 : {
				items: 3
			},

			1200 : {
				items: 3
			}
		}
	});

	$('.owl-doc-video-thumbs__item').on('click', function(e){
		var owl = $('.owl-doc-video');
		var $this = $(this);
		var index = $this.parent().index();

		owl.trigger('to.owl.carousel', [index, 200]);
	}); 

	/*
	* owl doc works
	*/

	$('.doc-main__works-owl').owlCarousel({
		nav:true,
		items: 1,
		margin: 0,
		singleItem: true,
		dots:true,
		loop: true
	});

	/*
	* banner close btn
	*/ 

	$('.full-banner__close-btn').on("click", function(e){
		e.preventDefault();
		var $this = $(this);

		$this.parents('.full-banner').fadeToggle(300);
	});

	/*
	* article-p__row-owl
	*/ 

	$('.article-p__row-owl').owlCarousel({
		nav:true,
		items: 1,
		margin: 0,
		singleItem: true,
		dots:false,
		loop: true
	});

	/*
	* validation
	*/

	var validation = (function(){

		var inputs = $("form").find('input, textarea'); //инпуты в формах

		var init = function(){
			setUpListeners();
		};

		var setUpListeners = function(){
			inputs.on('keydown', function(e){ 	        //удаляем ошибки при нажатии клавишь на поле ввода
				var $this = $(this);
				$this.parent().removeClass('error');
			});

			$("form").on("submit", function(e){
				e.preventDefault();

				( isValid($(this)) ) ? console.log("well done!") : console.log("valid fail!");
			});

			$(".modal-special__form").on("submit", function(e){
				e.preventDefault();

				if ( isValid($(this)) ) {
					if ( $('body').hasClass('modal-open') ){

						var showThxModal = function() {
							$('.modal').unbind('hidden.bs.modal', showThxModal);
							$('.modal-special-thx').modal('show')
						};

						$('.modal').on('hidden.bs.modal', showThxModal).modal('hide');

					} else {
						$('.modal-special-thx').modal();
					}
				};
			});
		};

		function isValid(thisForm){

			var isValid = true;

			thisForm.find('input, textarea').each(function(i){

				var $this = $(this),
				thisVal = $this.val();

				if ($this.prop('required')) {

					if ($this.is('input[type="email"]')) {

						if (validateEmail(thisVal)) {

							$this.parent().removeClass('error');
							return isValid = true;

						} else {

							$this.parent().addClass('error');
							return isValid = false;
						}

					}

					if (!thisVal.trim()) {

						$this.parent().addClass('error');
						return isValid = false;

					} else {

						$this.parent().removeClass('error');
						return isValid = true;

					}	

				}

			});

			return isValid;
		};

		function validateEmail(email){
			var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			return re.test(email);
		};

		return {
			init:init
		}
	}());

	$(document).ready(function(){
		validation.init();
	});

	/* 
	*  fixed nav while scroll
	*/

	if ( $('.doctor-p__bot').length ) {

		var menuItems = $('.doctor-p__nav-link');
		var scrollItems = menuItems.map(function(){
			var item = $($(this).attr("href"));
			if (item.length) { return item; }
		});
		var lastId;


		$(window).scroll(function() {
			var $nav = $('.doctor-p__bot');
			var $midElem = $('.doctor-p__mid');
			var statickPosition = $midElem[0].offsetTop + $midElem.outerHeight();
			var winScroll = window.pageYOffset;
			var navHeight = $nav.outerHeight()+15;
			var fromTop = $(this).scrollTop()+navHeight;

			var box = $midElem[0].getBoundingClientRect();
			var body = document.body;
			var docElem = document.documentElement;
			var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
			var clientTop = docElem.clientTop || body.clientTop || 0;
			var statickPosition = box.top +  scrollTop - clientTop + $midElem.outerHeight();

			if (winScroll > statickPosition) {
				$nav.addClass('fixed-nav-panel');
				$midElem.css('margin-bottom', '61px');
			} else {
				$nav.removeClass('fixed-nav-panel');
				$midElem.css('margin-bottom', '0');
			}

			var cur = scrollItems.map(function(){
				if ($(this).offset().top < fromTop)
					return this;
			});

			cur = cur[cur.length-1];
			var id = cur && cur.length ? cur[0].id : "";

			if (lastId !== id) {
				lastId = id;

				menuItems
				.parent().removeClass("active")
				.end().filter("[href='#"+id+"']").parent().addClass("active");
			}                   
		});
	};

	/*
	* inprice owl nav
	*/

	$(window).on('load', function (){
		$('.owl-p-inprice-nav').owlCarousel({
			nav:false,
			dots:false,
			margin:30,
			autoWidth:true
		});
	});

	$('.dropdown').on('show.bs.dropdown', function (e) {
		$(this).find('.dropdown-menu').first().stop(true, true).fadeIn(300);
	});

	$('.dropdown').on('hide.bs.dropdown', function (e) {
		$(this).find('.dropdown-menu').first().stop(true, true).fadeOut(300);
	});

	/*
	* inprice owl content
	*/

	$('.owl-p-inprice-content').owlCarousel({
		nav:true,
		dots:true,
		margin:30,
		loop: true,
		items:1,
		responsive : {
			768 : {
				items: 1
			},

			992 : {
				items: 2
			},

			1200 : {
				items: 2
			}
		}
	});

	$('.owl-p-price-content').owlCarousel({
		nav:false,
		dots:true,
		margin:30,
		loop: true,
		items:1
	});

	/*
	* manufacturer owl nav
	*/

	$(window).on('load', function (){
		$('.owl-manufacturer-nav').owlCarousel({
			nav:true,
			dots:false,
			margin:0,
			autoWidth:true
		});

		$('.imp-manufacturer__nav-link').on("click", function(e){
			var $this = $(this);

			$this.parents('.owl-manufacturer-nav').find('.imp-manufacturer__nav-link').removeClass('active');
			$this.addClass('active');
		});
	});

	/*
	* special badge modal
	*/ 

	$('.clinic-p__mid-special').hover(function() {
		var $this = $(this);
		$this.find('.clinic-p__mid-special-content').stop(true, true).delay(100).fadeIn(300, 'easeInOutCubic');
	}, function() {
		var $this = $(this);
		$this.find('.clinic-p__mid-special-content').stop(true, true).delay(100).fadeOut(300, 'easeInOutCubic');
	});

	/*
	* rework header menu tabs
	*/

	$('.header__drop-item').hover(function() {
		var $this = $(this);
		var id = $this.find('a').data('id');
		var tabs = $this.parent().parent().find('.tab-content .tab-pane');

		tabs.hide();
		$('.header__drop-item').removeClass('active');
		$this.addClass('active');
		$this.parent().parent().find('.tab-content').find(id).show();
	});

	/*
	* sticky sidebar banners
	*/

	$(window).load(function() {
		if ( $('.sticky').length ) {
			var elements = $('.sticky');
			Stickyfill.add(elements);
			
			$('.main').addClass('main-sticky');

			var $sidebarBanners = $('.sidebar-banners');
			var $sbContainer = $('.doc-content__sidebar.offers-content__sidebar');
			var $allSidebarElems = $('.doc-content__sidebar.offers-content__sidebar').children('*');
			var sbContainerHeight = $sbContainer.outerHeight();
			var allSeHeight = 0;
			var emptyHeight = 0;
			var bannersLen = $sidebarBanners.length;

			$allSidebarElems.each(function(index, el) {
				var $this = $(this);

				allSeHeight += $this.outerHeight(true);
			});

			emptyHeight = sbContainerHeight - allSeHeight - 45; /*расстояние отступа*/

			$sidebarBanners.each(function(index, el) {
				var $this = $(this);
				var height = emptyHeight/bannersLen + $this.outerHeight();

				$this.height(height);
			});
		}
	});

	/*
	* clinicks owl xs video
	*/

	$('.doc-main__xsvideo-owl').owlCarousel({
		nav:false,
		dots:true,
		margin:10,
		items:1,
		loop:true
	});

	/*
	* clinicks video modal
	*/

	$('body').on('click', '.doc-aside__video-link', function(e){
		e.preventDefault();

		var yootubeVideoId = $(this).attr('href');
		var videoFrame = '<iframe width="560" height="315" src="//www.youtube.com/embed/'+yootubeVideoId+'?autoplay=0" frameborder="0" allowfullscreen></iframe>';

		var modal = '<div class="modal fade modal-video modal_black"><div class="modal-video__wrap"><div class="modal-video__dialog"><a href="#" class="modal-video__close-btn" data-dismiss="modal"></a><div class="modal-video__container">'+videoFrame+'</div></div></div></div>';

		if ( $('.modal-video').length == 0 ) {
			$('body').append(modal);
		} else {
			$('.modal-video .modal-video__container').append(videoFrame);
		};

		$('.modal-video').modal();
		$('.modal-video').on('hidden.bs.modal', function () {
			$('.modal-video .modal-video__container').html('');
		});

		return false;
	}); 

	/* scroll-top */
	$(document).on('scroll', function(){
		if ($(window).scrollTop() > 1000) {
			$('.button-to-top').show();
		} else {
			$('.button-to-top').hide();
		};
	});

	$('.button-to-top').click(function(e){ 
		e.preventDefault();

		$("html, body").animate({ scrollTop: 0 }, 1000);
	});

	/****
	** attention modals
	****/

	$('.attention').hover(function() {
		var $this = $(this);
		$this.find('.attention-modal').stop(true, true).delay(100).fadeIn(300, 'easeInOutCubic');
	}, function() {
		var $this = $(this);
		$this.find('.attention-modal').stop(true, true).delay(100).fadeOut(300, 'easeInOutCubic');
	});

	/*
	* near owl
	*/

	$('.owl-near').owlCarousel({
		nav:false,
		dots:true,
		margin:30,
		items: 1,
		autoWidth: true,
		loop:true,
		responsive : {
			768 : {
				items: 1,
				autoWidth: false
			},

			992 : {
				items: 2,
				autoWidth: false
			},

			1200 : {
				items: 3,
				autoWidth: false
			}
		}
	});

	/*
	* akcii owl
	*/

	$('.owl-akcii').owlCarousel({
		nav:true,
		dots:false,
		margin:30,
		items: 2,
		autoWidth: false,
		loop:true
	});

	/*
	* owl doctorwork
	*/

	$('.owl-doctorwork').owlCarousel({
		nav:false,
		dots:true,
		margin:0,
		items: 1,
		autoWidth: false,
		loop:true
	});

	/*
	* card seti akcii
	*/

	$('.p-seti__akcii-tabs-item a').on('click', function(e){
		e.preventDefault();

		var $this = $(this);
		var $tabItem = $this.parent();
		var $tabItems = $('.p-seti__akcii-tabs-item');
		var filterID = $this.data('filter');
		var $contentItem = $('.p-seti__akcii-item');

		$tabItems.removeClass('active');
		$this.parent().addClass('active');

		if (filterID == "all") {
			$contentItem.show();
		} else {
			$contentItem.hide().filter("[data-filter="+filterID+"]").show();
		}
	});

	$('[data-hide-me]').on('click', function(e){
		e.preventDefault();

		$(this).hide();
	});

	/* 
	* rating table nav
	*/

	$(window).on('load', function (){
		$(".owl-rating-top-nav").owlCarousel({
			nav:false,
			dots:false,
			margin:0,
			autoWidth:true,

			onInitialized : function() {
				setTimeout(function () {
					var indx = $('.owl-rating-bot-nav').find('.owl-rating-top-nav__link.active').parents('.owl-item').index();
					$(".owl-rating-bot-nav").trigger('to.owl.carousel', indx-1);
				}, 300);
			}
		});

		$(".owl-rating-bot-nav").owlCarousel({
			nav:false,
			dots:false,
			margin:0,
			autoWidth:true,

			onInitialized : function() {
				setTimeout(function () {
					var indx = $('.owl-rating-bot-nav').find('.owl-rating-bot-nav__link.active').parents('.owl-item').index();
					$(".owl-rating-bot-nav").trigger('to.owl.carousel', indx-1);
				}, 300);
			}
		});
	});

	$('.rating-table tbody tr').hover(function() {
		var $this = $(this);

		$this.addClass('rating-table_tr-hover').prev().addClass('rating-table_tr-border-bottom');
	}, function() {
		var $this = $(this);

		$this.removeClass('rating-table_tr-hover').prev().removeClass('rating-table_tr-border-bottom');
	});

	/* rating table... end; */

	/*
	* contacts map
	*/

	if ( $('#p-contacts-map').length ) {
		ymaps.ready(initContactMaps);

		var mapOptions = {
			center: [55.7345391, 37.5648671],
			zoom: 15
		};

		function initContactMapByID(id){
			var myMap = new ymaps.Map(id, mapOptions, {
				searchControlProvider: 'yandex#search'
			});

			myMap.geoObjects.add(new ymaps.Placemark([55.7345391, 37.5648671], {}, {
				preset: 'islands#redDotIconWithCaption'
			}));
		};

		function initContactMaps(){
			initContactMapByID('p-contacts-map');
			initContactMapByID('p-contacts-map-xs');
		};
	};

	/* contacts map ... end; */

	/* new menu */

	var $nMenuToggler = $('.header-top__menu-btn');
	var $body = $('body');
	var $nMenu = $('.header__menu');

	$nMenuToggler.on('click', function(e){
		e.preventDefault();

		$body.addClass('body-overflow');
		$nMenu.addClass('active');
	});

	$(document).on('click touchstart', function(e){

		if (!$nMenu.is(e.target) && $nMenu.has(e.target).length === 0 && !$nMenuToggler.is(e.target) && $nMenuToggler.has(e.target).length === 0) { 
			$body.removeClass('body-overflow');
			$nMenu.removeClass('active');
		} 
	});

	/* help page datepicker */

	if ( $.datetimepicker ){
		$.datetimepicker.setLocale('ru');

		$('#p-help-t__time-from').datetimepicker({
			format: 'd M Y',
			onShow:function( ct ){
				this.setOptions({
					maxDate: $('#p-help-t__time-to').val() ? $('#p-help-t__time-to').val() : false,
					formatDate:'d M Y'
				})
			},
			onSelectDate:function(){
				$('#p-help-t__time-to').datetimepicker('show');
			},
			timepicker:false
		});

		$('#p-help-t__time-to').datetimepicker({
			format: 'd M Y',
			onShow:function( ct ){
				this.setOptions({
					minDate: $('#p-help-t__time-from').val() ? $('#p-help-t__time-from').val() : false,
					formatDate:'d M Y'
				})
			},
			timepicker:false
		});
	}

	/* help page datepicker...end; */

}());
